package com.epam;

import task.*;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of fibonacci :");
        int number = sc.nextInt();

        FibonacciBiggestNumber.fibonacciBiggestOdd(number);
        FibonacciBiggestNumber.fibonacciBiggestEven(number);

        FibonacciPercentage.percentageEven(number);
        FibonacciPercentage.percentageOdd(number);


        NumberOddEven.printOdd(Executable.intrvals());
        NumberOddEven.printEven(Executable.intrvals());

    }
}
