package task;

public class FibonacciPercentage extends Fibonacci {
    public static void percentageEven(int number){
        double count = 0;
        for(int i = 1; i<=number; i++){
            if(fibunacci(i)%2 == 0){
                count++;
            }
        }
        System.out.println("Percentage of even: " + count/number*100 + "%");
    }
    public static void percentageOdd(int number){
        double count = 0;
        for(int i = 1; i<=number; i++){
            if(fibunacci(i)%2 != 0){
                count++;
            }
        }
        System.out.println("Percentage of odd: " + count/number*100 + "%");
    }
}
