package task;

public class NumberOddEven {

    private static int sumEven = 0;
    private static int sumOdd = 0;

    public static void printEven(int [] numbers){
        int even;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                even = numbers[i];
                sumEven += even;
                System.out.print(even + " ");
            }
        }
        System.out.println();
        System.out.println(sumEven);
    }
    public static void printOdd(int [] numbers) {
        int odd;
        for (int j = 0; j < numbers.length; j++) {
            if (numbers[j] % 2 != 0) {
                odd = numbers[j];
                sumOdd += odd;
                System.out.print(odd + " ");
            }
        }
        System.out.println();
        System.out.println(sumOdd);
    }
}


