package task;

public class MaxMinNumber {

    public static void maxNumber(int [] num){
        int number = 0;
        for(int i = 0; i<num.length; i++){
            if(number < num[i]){
                number = num[i];
            }
        }
        System.out.println(number);
    }

    public static void minNumber(int [] num){
        int number = Integer.MAX_VALUE;
        for(int i = 0; i<num.length; i++){
            if(number > num[i]){
                number = num[i];
            }
        }
        System.out.println(number);
    }


}
