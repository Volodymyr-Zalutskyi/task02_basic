package task;

import java.util.Scanner;

public class Executable {

    public static int [] intrvals() {
        Scanner sc = new Scanner(System.in);
        int numberOfElement;
        System.out.println("Enter number of elements in interval");
        while (true) {
            numberOfElement = sc.nextInt();
            if (numberOfElement >= 0) {
                break;
            }
            System.out.println("Entered number should be positive. Try one more");
        }
        int[] intervals = new int[numberOfElement];
        System.out.print("Enter the interval which equals - " + numberOfElement + " elements");
        for (int i = 0; i < intervals.length; i++) {
            intervals[i] = sc.nextInt();
        }
        return intervals;
    }
}
