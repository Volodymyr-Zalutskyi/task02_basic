package task;

import java.security.InvalidParameterException;


public class Fibonacci {

    public static int fibunacci(int number){
        int first = 1;
        int second = 1;
        int temp;
        if(number == 1){
            return 1;
        }
        if(number <= 0){
            throw new InvalidParameterException("Sequence number can't be negative!");
        }
        for(int i = 2; i<=number; i++){
            temp = first + second;
            first = second;
            second = temp;
        }
        return second;
    }
}
